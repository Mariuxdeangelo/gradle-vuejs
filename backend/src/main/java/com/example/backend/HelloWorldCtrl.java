package com.example.backend;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloWorldCtrl {

    @GetMapping("/hello")
    public String helloWorld() {
        System.out.println("Hello World");
        return "Hello, World!";
    }
}